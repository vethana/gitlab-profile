<picture>
  <source media="(prefers-color-scheme: dark)" srcset="./assets/vethana-banner.png">
  <source media="(prefers-color-scheme: light)" srcset="./assets/vethana-banner.png">
  <img alt="Logo of Vethana in Green." src="./assets/vethana-banner.png">
</picture>

### 👋 Introduction

We are an end-to-end payroll management platform.

### 🎯 Objective

It is an end-to-end payroll management platform that will also comply with the rules, regulations, and guidelines of India and related industry standards.

It will consist of the following features:
    
- Seamless payroll calculation and disbursement based on various legal compliance and company policies.
- Sharing electronically signed payslips, tax statements, and full and final settlements securely with end users.

### 🌈 Contribution guidelines

We appreciate any and all contributions, and hence we have outlined contribution guidelines for each repo or project. 

It is done so because some require a basic signing requirement for contributions (we call it the **Level 1** signing requirement) and some require a special signing requirement for contributions (we call it the **Level 0** signing requirement).

We kindly request that you review each of them before you start working on the repo or project.

### 👩‍💻 Useful resources

#### Social:
  - Website: https://www.vethana.in
  - LinkedIn: https://www.linkedin.com/company/vethana
  - Mastodon: https://mastodon.social/@vethana

  > Note: We are only on above mentioned social platforms, and it is updated every minute.

#### Logo:

Vethana Logo in Green (#009E4D) |   Vethana Logo in White (#FFFFFF)
:-------------------------: |  :-------------------------:
![Vethana Logo in Green](./assets/vethana-logo.png) |  ![Vethana Logo in White](./assets/vethana-logo-white.png)

